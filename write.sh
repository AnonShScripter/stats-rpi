#!/usr/bin/env bash
# <port> <password>

tempe() { 
    /opt/vc/bin/vcgencmd measure_temp
    printf 'pmic_'; /opt/vc/bin/vcgencmd measure_temp pmic
    /opt/vc/bin/vcgencmd get_throttled
    /opt/vc/bin/vcgencmd measure_clock arm
    for i in AGIF FLAC H264 MJPA MJPB MJPG MPG2 MPG4 MVC0 PCM THRA VORB VP6 VP8 WMV9 WVC1;do
        /opt/vc/bin/vcgencmd codec_enabled "${i}"
    done
    /opt/vc/bin/vcgencmd display_power
    /opt/vc/bin/vcgencmd measure_volts core
    /opt/vc/bin/vcgencmd hdmi_status_show
    /opt/vc/bin/vcgencmd get_config int
    /opt/vc/bin/vcgencmd get_config str
    printf 'totalmem=%s\n' "$(cat /proc/meminfo | awk '-F:        ' '/^MemTotal/ {print $2}' | awk '{print $1}')"
    printf 'freemem=%s\n' "$(cat /proc/meminfo | awk '-F:         ' '/^MemFree/ {print $2}' | awk '{print $1}')"
    printf 'loadavg=%s\n' "$(cat /proc/loadavg)"
    printf 'cpu_temp=%s\n' "$(cat /sys/class/thermal/thermal_zone0/temp)"
};
echo -n "${2}">~/shitpass
exec 3<>~/shitpass

#cat /dev/urandom | tr -cd 'A-Za-z0-9' | fold -w100 | head -5000 
tempe |
xz --format=raw  -T0 '--lzma2=dict=4096,lc=0,lp=0,pb=0,mode=fast,nice=24,mf=hc3,depth=1000' |
aespipe -e aes256 -H sha512 -p3 |
ncat --send-only -l "${1}"
#wc -c

exec 3<>/dev/null
