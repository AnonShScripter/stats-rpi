#!/usr/bin/env bash
# <host> <port> <password> [args for xz]

rng() { tr -cd 'A-Za-z0-9-'</dev/urandom|fold -w"${1}"|head -1;}
pwf="$HOME/$(rng 32)"
dtf="${pwf}-1"
printf %s "${3}">"${pwf}"
exec 3<"${pwf}"

ncat --recv-only "${1}" "${2}" |
~/aespipe -e aes256 -H sha512 -dp3  |
xz -d --format=raw '--lzma2=dict=4096,lc=0,lp=0,pb=0,mode=fast,nice=24,mf=hc3,depth=1000' 2>/dev/null
exec 3<>/dev/null
rm "${pwf}" 
