#!/usr/bin/env bash
# same as read.sh
r() {
    printf '\033[31m%s\033[1m%s\033[0m\033[31m%s\033[0m' '[' "${*}" ']'
}

g() {
    printf '\033[32m%s\033[1m%s\033[0m\033[32m%s\033[0m' '[' "${*}" ']'
}

y() {
    printf '\033[33m%s\033[1m%s\033[0m\033[33m%s\033[0m' '[' "${*}" ']'
}

c() {
    printf '\033[K'
}

trap 'setterm -cursor on;exit 0' INT KILL HUP QUIT TERM
raw="$(./read.sh "${@}")"
test -z "${raw}" && exit 1

vctemp="$(awk '-F=' '/^temp=/ {print $2}'<<<"${raw}" | awk -F\' '{print $1}' )"
pmictemp="$(awk '-F=' '/^pmic_temp=/ {print $2}'<<<"${raw}" | awk -F\' '{print $1}' )"
cpufreq="$(awk '-F=' '/^frequency/ {print $2}'<<<"${raw}" | head -1)"
maxfreq="$(awk '-F=' '/^arm_freq=/ {print $2}'<<<"${raw}" | head -1)"
loadavg="$(awk '-F=' '/^loadavg=/ {print $2}'<<<"${raw}")"
throt="$(awk '-F=' '/^thrott/ {print $2}'<<<"${raw}")"
freq="$(awk '-F=' '/^freq/ {print $2}'<<<"${raw}")"

finalthrots=($(printf '%04d%04d%04d%04d%04d' $(for n in $(cut -c 3- <<<"${throt}" | fold -w1 | tr 'a-f' 'A-F');do echo "obase=2; ibase=16; $n" | bc;done )|rev|fold -w1))
disp="$(awk '-F=' '/^display_power/ {print $2}'<<<"${raw}")"

# performance
c;printf '%s\n%s\n%s\n' '┏━┓┏━╸┏━┓┏━╸┏━┓┏━┓┏┳┓┏━┓┏┓╻┏━╸┏━╸' '┣━┛┣╸ ┣┳┛┣╸ ┃ ┃┣┳┛┃┃┃┣━┫┃┗┫┃  ┣╸ ' '╹  ┗━╸╹┗╸╹  ┗━┛╹┗╸╹ ╹╹ ╹╹ ╹┗━╸┗━╸';c

printf '%-33s%s' 'VC Temperature' ' --> ';if test "${vctemp%.?}" -ge 35;then r "${vctemp}";else g "${vctemp}";fi;printf '\n'
printf '%-33s%s' 'PMIC Temperature' ' --> ';if test "${pmictemp%.?}" -ge 35;then r "${pmictemp}";else g "${pmictemp}";fi;printf '\n'
printf '%-33s%s' 'Load average' ' --> ';if test "$(awk '{print $1}'<<<"${loadavg%%.*}")" -ge 3;then r "${loadavg}";else g "${loadavg}";fi;printf '\n'
c;printf '%-33s%s' 'Current CPU Frequency' ' --> ';if test "$((cpufreq/1000000))" -lt "${maxfreq}";then y "$((cpufreq/1000000))";else g "$((cpufreq/1000000))";fi;c;printf '\n\n'

printf '%-33s%s' 'Acute Undervoltage' ' --> ';if test "${finalthrots[0]}" -eq '1';then r '!';else g ' ';fi;printf '\n'
printf '%-33s%s' 'Acute speed cap' ' --> ';if test "${finalthrots[1]}" -eq '1';then r '!';else g ' ';fi;printf '\n'
printf '%-33s%s' 'Acute throttling' ' --> ';if test "${finalthrots[2]}" -eq '1';then r '!';else g ' ';fi;printf '\n'
printf '%-33s%s' 'Acute Soft temp limit' ' --> ';if test "${finalthrots[3]}" -eq '1';then r '!';else g ' ';fi;printf '\n'
printf '%-33s%s' 'Past Undervoltage' ' --> ';if test "${finalthrots[16]}" -eq '1';then r '!';else g ' ';fi;printf '\n'
printf '%-33s%s' 'Past speed cap' ' --> ';if test "${finalthrots[17]}" -eq '1';then r '!';else g ' ';fi;printf '\n'
printf '%-33s%s' 'Past throttling' ' --> ';if test "${finalthrots[18]}" -eq '1';then r '!';else g ' ';fi;printf '\n'
printf '%-33s%s' 'Past Soft temp limit' ' --> ';if test "${finalthrots[19]}" -eq '1';then r '!';else g ' ';fi;printf '\n'

# hwdec
printf '%s\n%s\n%s\n' '╻ ╻╻ ╻╺┳┓┏━╸┏━╸' '┣━┫┃╻┃ ┃┃┣╸ ┃'  '╹ ╹┗┻┛╺┻┛┗━╸┗━╸'

for i in AGIF FLAC H264 MJPA MJPB MJPG MPG2 MPG4 MVC0 PCM THRA VORB VP6 VP8 WMV9 WVC1;do
    a="$(awk '-F=' '/^'"${i}"'/ {print $2}'<<<"${raw}")"
    printf '%-4s%s' "${i}" ' --> '
    if test "${a}" == enabled;then g '+';else r '-';fi
    printf '\n'
done
